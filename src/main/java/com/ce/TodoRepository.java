package com.ce;

import java.util.List;

import org.springframework.cloud.gcp.data.datastore.repository.DatastoreRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

@RepositoryRestResource(collectionResourceRel = "todos", path = "todos")
public interface TodoRepository extends DatastoreRepository<Todo, Long>{

	List<Todo> findByUser(String user);
	
	List<Todo> findByDoneFalse();
	
	List<Todo> findByUserAndDoneFalse(String user);

}
